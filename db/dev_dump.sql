-- MySQL dump 10.13  Distrib 5.5.20, for Win64 (x86)
--
-- Host: localhost    Database: podium
-- ------------------------------------------------------
-- Server version	5.5.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ns` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `source` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_on_ns_and_name` (`ns`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (3,'','test','\n\n<h3><a name=\"_\"></a><span class=\"mw-headline\" id=\"_\">Тестовая страничка</span></h3>\n\r\n\n\r\n\r\n\r\n<p><img src=\"/assets/logo.png\"></img>\r\n</p>\r\n<p>sdsdf sdf sdf  sfgs gsdgs d\r\n</p>\r\n<p>df adfh dfha dh </p>[]','=== Тестовая страничка ===\r\n\r\n{{test}}\r\n\r\n<img src=\'/assets/logo.png\'>\r\n\r\nsdsdf sdf sdf  sfgs gsdgs d\r\n\r\ndf adfh dfha dh ','2014-05-14 13:45:57','2014-05-19 09:26:33'),(5,'template','menu','\n\n\n\n\n\n\n\n\n<ul><li><a href=\"/about\">О клубе</a>\r</li><li><a href=\"/gallery\">Галерея</a>\r</li><li><a href=\"/events\">События</a>\r</li><li>Меню\r<ul><li><a href=\"/price\">Цены вход</a>\r</li><li><a href=\"/crazy_price\">Цены crazy меню</a>\r</li><li><a href=\"/bar\">Бар</a>\r</li></ul></li><li><a href=\"/partners\">Партнёры</a>\r</li><li><a href=\"/contacts\">Контакты</a>\r</li></ul>\n','* [[/about|О клубе]]\r\n* [[/gallery|Галерея]]\r\n* [[/events|События]]\r\n* Меню\r\n** [[/price|Цены вход]]\r\n** [[/crazy_price|Цены crazy меню]]\r\n** [[/bar|Бар]]\r\n* [[/partners|Партнёры]]\r\n* [[/contacts|Контакты]]\r\n','2014-05-15 13:15:06','2014-05-26 14:44:15'),(6,'','partners','\n\n<h1><a name=\"\"></a><span class=\"mw-headline\" id=\"\">Партнёры</span></h1>\n\r\n\n<p><div class=\"partners row\">\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/larotonda.png\" alt=\"La Rotonda\" title=\"La Rotonda\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/laterassa.png\" alt=\"La Terasse\" title=\"La Terasse\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/budweis.png\" alt=\"Bar Budweis\" title=\"Bar Budweis\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/gallery.png\" alt=\"Галерея\" title=\"Галерея\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/citybar.png\" alt=\"City bar\" title=\"City bar\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/cultura.png\" alt=\"Культура\" title=\"Культура\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/cayman.png\" alt=\"Cayman Interactive\" title=\"Cayman Interactive\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/mediaantlab.png\" alt=\"Media Ant Lab\" title=\"Media Ant Lab\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/alphabank.png\" alt=\"Альфа-банк\" title=\"Альфа-банк\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/davidoff.png\" alt=\"Davidoff\" title=\"Davidoff\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/aldo_coppola.png\" alt=\"Aldo Coppola\" title=\"Aldo Coppola\" style=\"\" /></div>\r\n  <div class=\"item col-md-3\"><img src=\"/assets/partners/manege.png\" alt=\"Манеж\" title=\"Манеж\" style=\"\" /></div>\r\n</div></p>','= Партнёры =\r\n<div class=\'partners row>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/larotonda.png|La Rotonda]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/laterassa.png|La Terasse]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/budweis.png|Bar Budweis]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/gallery.png|Галерея]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/citybar.png|City bar]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/cultura.png|Культура]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/cayman.png|Cayman Interactive]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/mediaantlab.png|Media Ant Lab]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/alphabank.png|Альфа-банк]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/davidoff.png|Davidoff]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/aldo_coppola.png|Aldo Coppola]]</div>\r\n  <div class=\'item col-md-3\'>[[File:/assets/partners/manege.png|Манеж]]</div>\r\n</div>','2014-05-15 13:38:34','2014-05-15 14:11:57'),(7,'','about','\n\n<h1><a name=\"_\"></a><span class=\"mw-headline\" id=\"_\">О клубе</span></h1>\n\r\n\n\r\n<p>Gentelmen’s Club Podium\r\n</p>\r\n<p>Это знаменитый уровень сервиса и качества, помноженный на бесшабашное русское веселье. Обращаем внимание наших гостей, что в клубе Podium так же действуют клубные карты всех стриптиз клубов России для бесплатного входа!\r\n</p>\r\n<p>В нашем клубе ежедневно для Вас работают лучшие танцовщицы со всей России.\r\n</p>\r\n<p>Почему мы?\r\n</p>\r\n<p>Мы знаем наверняка, чего ждут от настоящего стриптиза. Благодаря богатой истории мы научились создавать неповторимые шоу и тем самым приобрели множество поклонников.</p>','= О клубе =\r\n\r\nGentelmen’s Club Podium\r\n\r\nЭто знаменитый уровень сервиса и качества, помноженный на бесшабашное русское веселье. Обращаем внимание наших гостей, что в клубе Podium так же действуют клубные карты всех стриптиз клубов России для бесплатного входа!\r\n\r\nВ нашем клубе ежедневно для Вас работают лучшие танцовщицы со всей России.\r\n\r\nПочему мы?\r\n\r\nМы знаем наверняка, чего ждут от настоящего стриптиза. Благодаря богатой истории мы научились создавать неповторимые шоу и тем самым приобрели множество поклонников.','2014-05-15 14:13:09','2014-05-15 14:13:09'),(8,'','contacts','\n\n<h1><a name=\"\"></a><span class=\"mw-headline\" id=\"\">Контакты</span></h1>\n\r\n\n\r\n<p><img src=\"/assets/map.png\" alt=\"Карта\" title=\"Карта\" style=\"\" />\r\n</p>\r\n<p><div class=\"contacts row\">\r\n  <div class=\"col-md-3\">\r\n    <strong>Адрес</strong>\r\n     г. Тверь , пр-т. Ленина, 32а \r\n  </div>\r\n  <div class=\"col-md-3\">\r\n    <strong>Контактные телефоны</strong>\r\n     +7-919-063-8000\r\n  </div>\r\n  <div class=\"col-md-3\">\r\n    <strong>Режим работы</strong>\r\n     Пн-Пт: с 10-00 до 19-00.\r\n  </div>\r\n  <div class=\"col-md-3\">\r\n    <strong>Электронная почта</strong>\r\n     <a href=\"mailto:info@podium.ru\">info@podium.ru</a>\r\n  </div>\r\n</div></p>','= Контакты =\r\n\r\n[[File:/assets/map.png|Карта]]\r\n\r\n<div class=\'contacts row\'>\r\n  <div class=\'col-md-3\'>\r\n    <strong>Адрес</strong>\r\n     г. Тверь , пр-т. Ленина, 32а \r\n  </div>\r\n  <div class=\'col-md-3\'>\r\n    <strong>Контактные телефоны</strong>\r\n     +7-919-063-8000\r\n  </div>\r\n  <div class=\'col-md-3\'>\r\n    <strong>Режим работы</strong>\r\n     Пн-Пт: с 10-00 до 19-00.\r\n  </div>\r\n  <div class=\'col-md-3\'>\r\n    <strong>Электронная почта</strong>\r\n     info@podium.ru\r\n  </div>\r\n</div>','2014-05-15 14:19:12','2014-05-15 14:23:41'),(9,'','gallery','\n<p><div class=\"gallery_widget\"></div></p>','<div class=\'gallery_widget\'></div>','2014-05-20 12:28:25','2014-05-20 12:28:25'),(10,'','crazy_price','\n\n<h1><a name=\"Crazy_\"></a><span class=\"mw-headline\" id=\"Crazy_\">Crazy меню</span></h1>\n\r\n\n\r\n<p><div class=\"row\">\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/dance.png\"></img>\r\n  <span class=\"name\">Танец у стола</span>\r\n  <span class=\"subname\">(топлес)</span>\r\n  <span class=\"price\">1000</span>\r\n</div>\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/private_dance.png\"></img>\r\n  <span class=\"name\">Приватный танец</span>\r\n  <span class=\"price\">1500</span>\r\n</div>\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/private_dance_hall.png\"></img>\r\n  <span class=\"name\">Приватный танец в зале</span>\r\n  <span class=\"price\">2500</span>\r\n</div>\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/tequila.png\"></img>\r\n  <span class=\"name\">Текила любовь</span>\r\n  <span class=\"price\">3000</span>\r\n</div>\r\n</div>\r\n<div class=\"row\">\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/stage.png\"></img>\r\n  <span class=\"name\">Танец гостя на сцене</span>\r\n  <span class=\"price\">7000</span>\r\n</div>\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/music.png\"></img>\r\n  <span class=\"name\">Заказ музыки</span>\r\n  <span class=\"subname\">(1 песня)</span>\r\n  <span class=\"price\">3000</span>\r\n</div>\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/time.png\"></img>\r\n  <span class=\"name\">Продление работы клуба</span>\r\n  <span class=\"subname\">(на 1 час)</span>\r\n  <span class=\"price\">30 000</span>\r\n</div>\r\n<div class=\"crazy_item col-md-3\">\r\n  <img src=\"/assets/crazy/fantasy.png\"></img>\r\n  <span class=\"name\">Тайная фантазия</span>\r\n  <span class=\"price\">500</span>\r\n</div>\r\n</div>\r\n</p>\r\n<p>К оплате принимаются карты VISA & Master Card\r</p>','= Crazy меню =\r\n\r\n<div class=\'row\'>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/dance.png\' />\r\n  <span class=\'name\'>Танец у стола</span>\r\n  <span class=\'subname\'>(топлес)</span>\r\n  <span class=\'price\'>1000</span>\r\n</div>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/private_dance.png\' />\r\n  <span class=\'name\'>Приватный танец</span>\r\n  <span class=\'price\'>1500</span>\r\n</div>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/private_dance_hall.png\' />\r\n  <span class=\'name\'>Приватный танец в зале</span>\r\n  <span class=\'price\'>2500</span>\r\n</div>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/tequila.png\' />\r\n  <span class=\'name\'>Текила любовь</span>\r\n  <span class=\'price\'>3000</span>\r\n</div>\r\n</div>\r\n<div class=\'row\'>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/stage.png\' />\r\n  <span class=\'name\'>Танец гостя на сцене</span>\r\n  <span class=\'price\'>7000</span>\r\n</div>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/music.png\' />\r\n  <span class=\'name\'>Заказ музыки</span>\r\n  <span class=\'subname\'>(1 песня)</span>\r\n  <span class=\'price\'>3000</span>\r\n</div>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/time.png\' />\r\n  <span class=\'name\'>Продление работы клуба</span>\r\n  <span class=\'subname\'>(на 1 час)</span>\r\n  <span class=\'price\'>30 000</span>\r\n</div>\r\n<div class=\'crazy_item col-md-3\'>\r\n  <img src=\'/assets/crazy/fantasy.png\' />\r\n  <span class=\'name\'>Тайная фантазия</span>\r\n  <span class=\'price\'>500</span>\r\n</div>\r\n</div>\r\n\r\nК оплате принимаются карты VISA & Master Card\r\n','2014-05-23 13:51:33','2014-05-23 14:31:17'),(11,'','price','\n\n<h1><a name=\"\"></a><span class=\"mw-headline\" id=\"\">Вход</span></h1>\n\r\n\n\r\n<p><div class=\"menu_info row\">\r\n  <div class=\"price_block col-md-4\">\r\n     <img src=\"/assets/women.png\"></img>\r\n     Стоимость входа для женщин\r\n     <div class=\"price\">1000</div>\r\n  </div>\r\n  <div class=\"large col-md-4\">\r\n   полюбоваться красивыми танцами и уйти в реальность, \r\n   о которой Вы так долго мечтали.\r\n  <img src=\"/assets/logo_light.png\"></img>\r\n  </div>\r\n  <div class=\"price_block col-md-4\">\r\n     <img src=\"/assets/men.png\"></img>\r\n     Стоимость входа для мужчин\r\n     <div class=\"price\">500</div>\r\n  </div>\r\n</div>\r\n</p>\r\n<p>Зачем скучать в одиночестве дома или в обычном баре, если можно посетить жаркое местечко под названием стриптиз-клуб. Здесь не знают, что такое тоска или скука. В любое время дня и ночи у нас интересно и весело. А как же может быть иначе, если в этом месте собраны только самые знойные и сексапильные девушки страны. Они красивы, молоды, внимательны и могут своими отточенными плавными движениями свести с ума любого представителя сильного пола. Многие именно за этим и приходят на женский стриптиз, чтобы отрешиться от будничной атмосферы и несколько часов провести в королевстве грез.</p>','= Вход =\r\n\r\n<div class=\'menu_info row\'>\r\n  <div class=\'price_block col-md-4\'>\r\n     <img src=\'/assets/women.png\'/>\r\n     Стоимость входа для женщин\r\n     <div class=\'price\'>1000</div>\r\n  </div>\r\n  <div class=\'large col-md-4\'>\r\n   полюбоваться красивыми танцами и уйти в реальность, \r\n   о которой Вы так долго мечтали.\r\n  <img src=\'/assets/logo_light.png\'/>\r\n  </div>\r\n  <div class=\'price_block col-md-4\'>\r\n     <img src=\'/assets/men.png\'/>\r\n     Стоимость входа для мужчин\r\n     <div class=\'price\'>500</div>\r\n  </div>\r\n</div>\r\n\r\nЗачем скучать в одиночестве дома или в обычном баре, если можно посетить жаркое местечко под названием стриптиз-клуб. Здесь не знают, что такое тоска или скука. В любое время дня и ночи у нас интересно и весело. А как же может быть иначе, если в этом месте собраны только самые знойные и сексапильные девушки страны. Они красивы, молоды, внимательны и могут своими отточенными плавными движениями свести с ума любого представителя сильного пола. Многие именно за этим и приходят на женский стриптиз, чтобы отрешиться от будничной атмосферы и несколько часов провести в королевстве грез.','2014-05-27 12:24:44','2014-05-27 12:53:52');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `image_uid` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_photos_on_page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `preview` text,
  `text` text,
  `source` text,
  `datetime` datetime DEFAULT NULL,
  `image_uid` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (3,'events','Ntcnjsdfsdf','sdgasfgasfgadfg',NULL,'dfglkdfjgldkjflg kdjflg dfg\r\ndf hsadfh adfh adf hdfh','2014-05-26 13:57:00','2014/05/26/4z4qmfouij_uqHI5VnlSNw.jpg','2014-05-26 13:57:41','2014-05-26 13:57:41'),(4,'events','ghfghdfjsfgj','fdgsfgsfgn','\n<p>fgnfnfgnfgnfgn d dfg asfg df\r\n</p>\r\n<p>gsdf sgdfgsdfgs df gdfg sd\r\n</p>\r\n<p>sd fgsdfh sdfh adf adfh afdh </p>','fgnfnfgnfgnfgn d dfg asfg df\r\n\r\ngsdf sgdfgsdfgs df gdfg sd\r\n\r\nsd fgsdfh sdfh adf adfh afdh ','2014-05-26 14:08:00','2014/05/26/6m56yh8inx_700x1050.jpeg','2014-05-26 14:09:24','2014-05-26 14:22:38'),(5,'announces','lkljlkjlkj','lhljhkjhk','\n<p>jkhjkhjkhkh</p>','jkhjkhjkhkh','2013-05-26 15:02:00','2014/05/27/4mt4x4mtx6_0_a19a7_83523cd0_orig.jpg','2014-05-26 15:04:38','2014-05-27 12:10:27'),(6,'announces','ln,mn,mn,m',',mn,mn,mn,mn','\n<p>uiigi ig \r\n</p>\r\n<p>ihkjhkjhkjhjk</p>','uiigi ig \r\n\r\nihkjhkjhkjhjk','2014-05-26 15:10:00','2014/05/26/8a0zitjbx6_vgn.jpg','2014-05-26 15:10:38','2014-05-26 15:15:35'),(7,'announces','jjkhkjhk','jkhkjhlkhlkjh','\n<p>lkhklhlkhlkj hlkj hlk kjlh lk</p>','lkhklhlkhlkj hlkj hlk kjlh lk','2014-05-27 12:10:00','2014/05/27/2irlsl90dr_0_a19c3_c42f1cd7_orig.jpg','2014-05-27 12:11:17','2014-05-27 12:11:17'),(8,'announces','lkjhkljhlkj hlk','kjhkljh lkjhlkj hlk','\n<p>kjhkl hkl hkl glk glk</p>','kjhkl hkl hkl glk glk','2014-05-27 12:11:00','2014/05/27/5pjvsytv1_0_d2e43_ed2aec8_orig.jpg','2014-05-27 12:11:40','2014-05-27 12:11:40');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20140514070118'),('20140515133118'),('20140521092358'),('20140526094914');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `password_salt` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'fpaint','$2a$10$k..WoLPyRq04cRZK/oFKW.bLSW1EQQgIu6VyQFLS7mvii4TKnro9y','$2a$10$k..WoLPyRq04cRZK/oFKW.','2014-05-19 14:48:53','2014-05-19 14:52:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-28 10:33:37
