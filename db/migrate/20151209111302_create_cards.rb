class CreateCards < ActiveRecord::Migration
  def change
    drop_table :cards if table_exists? :cards
    create_table :cards do |t|
      t.string :parent
      t.string :title
      t.text :text
      t.string :image_uid
      t.integer :position      
      t.timestamps
    end
  end
end
