class CreatePage < ActiveRecord::Migration
  def up    
    unless table_exists? :pages 
      create_table :pages do |t| 
        t.string :ns
        t.string :name
        t.text :text
        t.text :source
        t.timestamps
      end 
    end  
  end

  def down
    drop_table :pages if table_exists? :pages
  end
end
