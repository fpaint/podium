class CreatePosts < ActiveRecord::Migration
  def change
    drop_table :posts if table_exists? :posts
    create_table :posts do |t|
      t.string :parent
      t.string :title
      t.text :preview
      t.text :text
      t.text :source
      t.datetime :datetime
      t.string :image_uid
      t.timestamps
    end
  end

end
