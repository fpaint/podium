class CreateMenu < ActiveRecord::Migration
  def change
    drop_table :menus if table_exists? :menus
    create_table :menus do |t|
      t.string :parent
      t.string :title
      t.text :href
      t.text :text
      t.string :image_uid
      t.integer :position
      t.timestamps
    end
  end
end
