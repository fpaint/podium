class CreateModels < ActiveRecord::Migration
  def up
    add_index :pages, [:ns, :name], unique: true

    unless table_exists? :photos
      create_table :photos do |t|
        t.integer :page_id
        t.string :image_uid
        t.integer :position
        t.timestamps
      end
    end
    add_index :photos, :page_id

    unless table_exists? :posts
      create_table :posts do |t|
        t.integer :page_id
        t.string :title
        t.text :preview
        t.date :date
        t.string :image_uid
        t.text :text
        t.timestamps
      end
    end
    add_index :posts, :page_id

  end

  def down
    drop_table :photos if table_exists? :photos
    drop_table :posts if table_exists? :posts
  end
end
