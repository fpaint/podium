Date::DATE_FORMATS[:default] = "%d.%m.%Y"
Time::DATE_FORMATS[:default] = "%d.%m.%Y %H:%M"
Date::DATE_FORMATS[:mysql_date] = "%Y-%m-%d"
Time::DATE_FORMATS[:mysql_date] = "%Y-%m-%d"