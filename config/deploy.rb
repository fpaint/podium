require 'rvm/capistrano'
require 'bundler/capistrano'
require 'capistrano-db-tasks'

load 'deploy/assets'

set :rvm_type, :system
set :rvm_path, '/usr/local/rvm'

set :rails_env, 'production'

# role :app, "tigerwork.ru"                          # This may be the same as your `Web` server
# role :db,  "podium", :primary => true # This is where Rails migrations will run
# set :user, "tigerwork"

set :application,     'podiumhall.com'
set :deploy_server,   'podiumhall.com'
set :user,            'podiumhall'

ssh_options[:forward_agent] = true


set :use_sudo,        false
set :deploy_to,       "/var/www/podiumhall/data/www/projects/#{application}"

set :bundle_dir,      "#{deploy_to}/shared/gems"
role :web,            deploy_server
role :app,            deploy_server
role :db,             deploy_server, primary: true

set :rvm_ruby_string, '1.9.3'
set :rake,            "rvm use #{rvm_ruby_string} do bundle exec rake"
set :bundle_cmd,      "rvm use #{rvm_ruby_string} do bundle"
set :bundle_without,  [:development, :test]
set :bundle_flags,        '--quiet'

set :scm, :none
set :deploy_via, :copy
# set :copy_strategy, :export
set :copy_exclude, '.git/*'
set :repository, '.'
set :local_repository, "#{deploy_server}:"

set :db_config, "#{shared_path}/database.yml"

set :assets_dir, 'public/uploads'
set :local_assets_dir, 'public/uploads'


after 'deploy:setup', :upload_database_config
task :upload_database_config, roles: :app do
  upload 'config/database.yml', db_config
end

after 'set_current_release', :copy_database_config
task :copy_database_config, roles: :app do
  db_config = "#{shared_path}/database.yml"
  run "cp #{db_config} #{current_release}/config/database.yml"
end


before 'deploy:finalize_update', :set_current_release
task :set_current_release, roles: :app do
    set :current_release, latest_release
end

# if you want to clean up old releases on each deploy uncomment this:
after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, roles: :app do
    run "touch #{current_path}/tmp/restart.txt"
  end
end
