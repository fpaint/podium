Podium::Application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'

  root :to => 'pages#show', defaults: {id: 'home'}

  namespace :api do
    resources :gallerys, only: [:show, :destroy], :defaults => { :format => 'json' } do
      resources :images, only: [:index], :defaults => { :format => 'json' }
    end
    resources :images, only: [:create, :show, :destroy], :defaults => { :format => 'json' }
    endpoint = Dragonfly.app.endpoint { |params, app|
      app.fetch_file("public/system/dragonfly/#{Rails.env}/#{params[:file]}").thumb(params[:size]) 
    }   
    get "image" => endpoint, as: :thumb
    post 'login' => 'sessions#create', as: :login
    post 'logout' => 'sessions#destroy', as: :logout

    get 'events' => 'events#index', as: :events, :defaults => { :format => 'json', :parent => 'events' }
    get 'announces' => 'events#index', as: :announces, :defaults => { :format => 'json', :parent => 'announces' }
    get 'announces/nearly' => 'events#nearly', as: :nearly_announces, :defaults => { :format => 'json', :parent => 'announces' }
    post 'request' => 'cards#card_request', :defaults => { :format => 'json' }
  end

  # post '/upload' => 'upload#create', as: :upload

  %w(events promotions).each do |name|
    scope "/#{name}" do
      resources :events, defaults: {parent: name}, path: '', as: name do
      end
    end
  end

  scope module: :events do
    resources :events, only: [] do
      resources :images, only: [:index, :destroy] do
        post :bulk_create, on: :collection
      end
    end
  end

  # scope parent: 'events'
  # resources 'events', defaults: { parent: 'events' } 
  #   resources :promotions, defaults: { parent: 'promotions' }, controller: :events
  resources 'announces', defaults: { parent: 'announces' }

  resources :background_images, only: [:index, :destroy] do
    post :bulk_create, on: :collection
  end

  resource :adult_confirmation, controller: :adult_confirmation, only: :show do
    get :confirm, on: :member
  end

  def page_mode(url, action, name, mode=:get)
    match "*id/#{url}" => action, via: [mode], as: name
  end

  post '/pages' => 'pages#update', as: :update_page
  post '/pages/preview' => 'pages#preview', as: :preview_page

  page_mode('menu_form', 'menu#form', :new_menu)
  page_mode('menu_form', 'menu#create', :create_menu, :post)
  page_mode('menu_form/(:menu_id)', 'menu#form', :edit_menu)
  page_mode('menu_form/(:menu_id)', 'menu#update', :update_menu, :put)
  page_mode('menu_form/(:menu_id)', 'menu#destroy', :destroy_menu, :delete)

  page_mode('card_form', 'cards#form', :new_card)
  page_mode('card_form', 'cards#create', :create_card, :post)
  page_mode('card_form/(:card_id)', 'cards#form', :edit_card)
  page_mode('card_form/(:card_id)', 'cards#update', :update_card, :put)
  page_mode('card_form/(:card_id)', 'cards#destroy', :destroy_card, :delete)

  match '*id' => 'pages#show', via: [:get], as: :page
  
end
