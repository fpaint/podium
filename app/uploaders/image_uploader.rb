class ImageUploader < BaseUploader
  version :thumb do
    process resize_to_fit: [200, 120]
  end

  version :slider_normal do
    process resize_to_fit: [850, 566]
  end

  version :slider_mini do
    process resize_to_fit: [135, 114]
  end
end
