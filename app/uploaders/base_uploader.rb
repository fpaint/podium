class BaseUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "system/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_white_list
    %w(jpg jpeg png gif)
  end


  def filename
    if model && model.read_attribute(mounted_as).present?
      model.read_attribute(mounted_as)
    elsif original_filename.present?
      "#{secure_token}.#{file.extension}"
    else
      super
    end
  end

protected

  def secure_token
    variable_name = :"@#{mounted_as}_secure_token"
    uuid = SecureRandom.uuid
    model.instance_variable_get(variable_name) || model.instance_variable_set(variable_name, uuid)
  end
end
