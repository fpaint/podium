class RequestMailer < ActionMailer::Base
  default from: "no-reply@podiumhall.com"
  default to: Proc.new { mailto_address }

  def request_mail(name, phone)
    @name = name
    @phone = phone
    mail(subject: "Запрос на карту клиента: #{@name} (#{@phone})")
  end

private

  def mailto_address
    ['fpaint@gmail.com', 'g_clubpodium@list.ru']
  end

end
