class Page < ActiveRecord::Base 
  class WikiParser < WikiCloth::Parser

    template do |template|    
      get_template template
    end

    def get_template(template)    
      temp = Page.where(ns: 'template', name: template).first
      temp.nil? ? "[#{template}]" : temp.text.html_safe
    end  

  end

  attr_accessible :source, :text

  TYPES = {photo_gallery: 9, video_gallery: 12}

  attr_accessor :children

  def set_source(source)
    update_attribute(:source, source)    
    update_attribute(:text, Page.parse(source))
  end

  def url
    ns == '' ? "#{name}" : "#{ns}:#{name}"
  end

  def self.parse(text)
    parser = WikiParser.new({data: text})
    parser.to_html(noedit: true) 
  end

  def self.template_text(name)
    temp = Page.where(ns: 'template', name: name).first
    return temp.nil? ? '' : temp.text.html_safe
  end

  TYPES.each do |type, page_id|
    define_singleton_method type do
      Page.find(page_id) if page_id.present?
    end

    define_method "#{type}_page?" do
      page_id == id
    end
  end

  def self.in_menu
    items = [:about, {:gallery_menu => [:interior, :gallery, :video]}, :promotions, :events, {:menu => [:price, :bar, :kitchen, :hookah, :crazy_price]}, :partners, :contacts]

    pages = []
    items.each do |item|
      if item.is_a?(Hash)
        item.each do |k, v|
          page = Page.where(:name => k).first
          page.children = []
          v.each {|pp| page.children << Page.where(:name => pp).first}
          page.children.delete_if {|pp| pp.nil? }
          pages << page
        end
      else
        page = Page.where(:name => item).first
        pages << page
      end
    end
    pages
  end
end
