class Photo < ActiveRecord::Base 
  include Rails.application.routes.url_helpers
  
  dragonfly_accessor :image
  attr_accessible :image, :image_uid, :page_id

  def to_jq_upload
    {
      "id" => id,
      "name" => read_attribute(:image_name),
      "size" => image.size,
      "url" => image.url,
      "thumbnail_url" => image.thumb('80x80#').url,
      "delete_url" => api_image_path(:id => id),
      "delete_type" => "DELETE"
    }
  end
end