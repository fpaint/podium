# encoding: utf-8
class Card < ActiveRecord::Base

  dragonfly_accessor :image
  attr_accessible :image, :image_uid, :parent, :title, :text, :position

end
