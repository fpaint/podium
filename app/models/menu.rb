# encoding: utf-8
class Menu < ActiveRecord::Base

  dragonfly_accessor :image
  attr_accessible :image, :image_uid, :parent, :title, :href, :text, :position

end
