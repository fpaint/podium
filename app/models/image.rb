class Image < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  attr_accessible :image, :imageable

  belongs_to :imageable, polymorphic: true
end
