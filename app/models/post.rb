# encoding: utf-8
class Post < ActiveRecord::Base
  has_many :images, as: :imageable, :dependent => :destroy

  dragonfly_accessor :image
  attr_accessible :image, :image_uid, :title, :preview, :source, :datetime

  def text
    source
  end

  def self.parse(text)
    parser = WikiCloth::Parser.new({data: text})
    parser.to_html(noedit: true) 
  end

  def day 
    datetime.strftime('%d')
  end

  def month    
    ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'][datetime.month-1]
  end

end
