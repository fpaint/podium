class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      can [:read, :edit, :delete], :all
    else
      cannot :read, :template
      cannot [:edit, :delete], :all
    end
  end
end
