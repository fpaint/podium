class ImageDecorator < Draper::Decorator
  delegate_all

  def to_jq_upload
    {
      "id" => source.id,
      # "name" => background_image.,
      # "size" => background_image.size,
      "url" => source.image.url,
      "thumbnail_url" => source.image.thumb.url,
      "delete_url" => h.event_image_path(source.imageable_id, source.id),
      "delete_type" => "DELETE"
    }
  end
end
