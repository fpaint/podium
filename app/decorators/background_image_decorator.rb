class BackgroundImageDecorator < Draper::Decorator
  delegate_all

  def to_jq_upload
    {
      "id" => id,
      # "name" => background_image.,
      # "size" => background_image.size,
      "url" => image.url,
      "thumbnail_url" => image.thumb.url,
      "delete_url" => h.background_image_path(:id => id),
      "delete_type" => "DELETE"
    }
  end
end
