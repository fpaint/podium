# encoding: utf-8
class MenuController < ApplicationController

  def form
    authorize! :edit, Menu
    @menu = params[:menu_id] ? Menu.find(params[:menu_id]) : Menu.new
  end

  def create
    authorize! :edit, Menu
    @menu = Menu.new params[:menu]
    @menu.parent = params[:id]
    @menu.save
    redirect_to page_path(params[:id])
  end

  def update
    authorize! :edit, Menu
    @menu = Menu.find(params[:menu_id])
    @menu.update_attributes params[:menu]
    redirect_to page_path(params[:id])
  end

  def destroy
    authorize! :edit, Menu
    @menu = Menu.delete(params[:menu_id])   
    render json: ['OK']
  end

end
