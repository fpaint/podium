# encoding: utf-8
class EventsController < ApplicationController

  def index
    @events = Post.where(parent: params[:parent]).order('datetime DESC').limit(10)
  end
  
  def new
    authorize! :edit, Post
    @event = Post.new 
  end

  def create
    authorize! :edit, Post
    @post = Post.new params[:post]
    @post.parent = params[:parent]
    @post.save
    redirect_to collection_path
  end

  def edit
    authorize! :edit, Post
    @event = Post.find(params[:id])
  end

  def update
    authorize! :edit, Post
    post = Post.find(params[:id])
    if post.update_attributes(params[:post])
      redirect_to resource_path(post)
    else 
      render "edit"  
    end
  end

  def destroy
    authorize! :edit, Post
    Post.where(id: params[:id]).destroy_all
    render json: 'OK'
  end

  def show
    @event = Post.find(params[:id])
  end

private

  def collection_path
    send("#{params[:parent]}_path")
  end

  def resource_path(event)
    send("#{params[:parent].singularize}_path", event)
  end
end 
