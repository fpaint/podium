# encoding: utf-8
class AnnouncesController < EventsController

  def create
    authorize! :edit, Post
    @post = Post.new params[:post]
    @post.parent = params[:parent]
    @post.save
    redirect_to announces_path
  end

  def update
    authorize! :edit, Post
    post = Post.find(params[:id])
    if post.update_attributes(params[:post])
      redirect_to announce_path(post)
    else 
      render "edit"  
    end
  end


end
