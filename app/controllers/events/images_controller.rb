class Events::ImagesController < ApplicationController
  before_filter :require_login
  inherit_resources

  before_filter :load_event, only: [:bulk_create, :destroy]

  respond_to :js, only: [:destroy]

  def bulk_create
    files = []
    params[:images].each do |bg_image_data|
      bg_image = Image.new(image: bg_image_data, imageable: @event).decorate
      if bg_image.save
        files << bg_image.to_jq_upload
      else
        render :json => [{:error => "custom_failure"}], :status => 304
        return
      end
    end
    render json: {files: files}, status: :created
  end

  def destroy
    @image = @event.images.find(params[:id])
    @image.destroy
    respond_with @image, location: nil
  end

private

  def load_event
    @event = Post.find(params[:event_id])
  end
end
