class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :adult_confirm?

  helper_method :current_user, :current_client

protected

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end 


  def adult_confirm?
    unless cookies[:adult_confirm]
      redirect_to adult_confirmation_path
    end
  end

  def require_login
    redirect_to root_path if current_user.blank?
  end
end
