class AdultConfirmationController < ApplicationController
  skip_before_filter :adult_confirm?

  def show
    render layout: false
  end

  def confirm
    cookies[:adult_confirm] = true
    redirect_to root_path
  end
end
