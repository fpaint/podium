# encoding: utf-8
class CardsController < ApplicationController

  def form
    authorize! :edit, Card
    @card = params[:card_id] ? Card.find(params[:card_id]) : Card.new
  end

  def create
    authorize! :edit, Card
    @card = Card.new params[:card]
    @card.parent = params[:id]
    @card.save
    redirect_to page_path(params[:id])
  end

  def update
    authorize! :edit, Card
    @card = Card.find(params[:card_id])
    @card.update_attributes params[:card]
    redirect_to page_path(params[:id])
  end

  def destroy
    authorize! :edit, Card
    @card = Card.delete(params[:card_id])   
    render json: ['OK']
  end

  def card_request
    RequestMailer.request_mail(params[:name], params[:phone]).deliver
    render json: ['OK']
  end

end
