# encoding: utf-8
class PagesController < ApplicationController
  
  def gallery
  end

  def show    
    parse_page_id
    @page = Page.where({ns: @ns, name: @name}).first_or_initialize
    # puts '@@@', @ns, @name, Page.where({ns: @ns, name: @name}).first
    @text = templated_text(@page.text)
    respond_to do |format|
      format.html
      format.json { render json: {
        ns: @ns,
        name: @name,
        text: @page.text
        }
      }
    end
  end

  def update
    authorize! :edit, Page
    parse_page_id    
    page = Page.where({ns: @ns, name: @name}).first_or_create
    page.update_attributes(text: params[:text])
    render json: page.to_json(only: [:id, :text])
  end

  def preview
    @text = templated_text(params[:text])
    render json: {text: @text}
  end

private 

  def templated_text text
    return unless text
    text.gsub /\{\{([\w]+)\}\}/ do |match|
      begin
        render_to_string(partial: "page_partials/#{$1}") 
      rescue ActionView::MissingTemplate => e
        template = Page.where({ns: 'template', name: $1}).first
        template ? template.text : "Error: template '#{$1}' not found"
      end
    end
  end
  
  def parse_page_id    
    @name = params[:id].gsub(/^\//, '')
    @ns = params[:ns] || ''
    (@ns, @name) = @name.split(':', 2) if(@name.include? ':')   
    @id = @ns.empty? ? @name : "#{@ns}:#{@name}"
    params[:id] = @ns.empty? ? @name : "#{@ns}_#{@name}"
  end  

end 
