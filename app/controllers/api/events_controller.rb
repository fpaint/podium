# encoding: utf-8
class Api::EventsController < ApplicationController
  
  def index
    @events = Post.where(parent: params[:parent]).order('datetime DESC').limit(2)
    render json: @events.as_json({only: [:id, :image_uid, :title, :preview], methods: [:day, :month]})
  end

  def nearly
    @events = Post.where({parent: params[:parent]}).where('datetime > NOW()').order('datetime').limit(10)
    render json: @events.as_json({only: [:id, :image_uid, :title, :preview], methods: [:day, :month]})
  end

end
