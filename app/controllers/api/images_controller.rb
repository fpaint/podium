# encoding: utf-8
class Api::ImagesController < ApplicationController

  def create    
    files = []
    params[:photo][:image].each do |image|
      photo = Photo.new({page_id: params[:photo][:page_id], image: image})      
      if photo.save
        files << photo.to_jq_upload
      else
        render :json => [{:error => "custom_failure"}], :status => 304  
        return
      end
    end
    render json: {files: files}, status: :created

   # @photo = Photo.new params[:photo]
   # if @photo.save
   #   respond_to do |format|
   #     format.html {
   #       render :json => [@photo.to_jq_upload].to_json,
   #       :content_type => 'text/html',
   #       :layout => false
   #     }
   #     format.json {
   #       render json: {files: [@photo.to_jq_upload]}, status: :created
   #     }
   #   end
   # else
   #   render :json => [{:error => "custom_failure"}], :status => 304
   # end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    render :json => true
  end

end
