# encoding: utf-8
class Api::GallerysController < ApplicationController
  def show
    list = Photo.where({page_id: params[:id]}).order(:position).all
    photos = list.map do |photo|
      {id: photo.id, uid: photo.image_uid, url: api_thumb_path(file: photo.image_uid, size: '134x113#')}
    end
    respond_to do |format|
      format.json {render :json => photos }
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy
    render json: {}
  end
end
