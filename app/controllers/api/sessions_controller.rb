# encoding: utf-8
class Api::SessionsController < ApplicationController

  def create
    user = User.authenticate(params[:login], params[:password])
    if user
      session[:user_id] = user.id
      render json: {message: 'OK'}, status: 200 
    else
      render json: {message: 'Неверный логин или пароль'}, status: 400 
    end
  end

  def destroy
    session[:user_id] = nil
    render json: {message: 'OK'}, status: 200 
  end 

end  