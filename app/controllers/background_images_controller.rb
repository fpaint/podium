class BackgroundImagesController < ApplicationController
  before_filter :require_login
  inherit_resources
  actions :index, :destroy

  def bulk_create
    files = []
    params[:background_images].each do |bg_image_data|
      bg_image = BackgroundImage.new(image: bg_image_data).decorate
      if bg_image.save
        files << bg_image.to_jq_upload
      else
        render :json => [{:error => "custom_failure"}], :status => 304
        return
      end
    end
    render json: {files: files}, status: :created
  end
end
