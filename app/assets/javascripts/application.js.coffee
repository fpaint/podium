#= require jquery
#= require jquery_ujs

#= require bootstrap
#= require js-routes
#= require_tree ./templates
#= require application/podium
#= require application/wiki
#= require jquery-fileupload

#= require_tree ./application

