window.App ||= {}

class App.Events

  constructor: (el)->
    @el = el
    @show_events()

  show_events: ->
    $.get Routes.api_events_path(), (res)=>     
      @el.html(JST['templates/wiki/events_slider']({items: res}))
      $('.slider', @el).carouFredSel
        items: 3
        width: 840
        align: 'center'        
        prev: 
          button: $('.prev', @el)
        next: 
          button: $('.next', @el)
        scroll:
          items: 1
        auto:
          play: false

# $ ->
#   el = $('.events_list')  

#   if el.length > 0
#     announces = new App.Events(el)
