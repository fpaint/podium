window.App ||= {}

class App.EditorPage

  show: (text)->
    page = $("<div>").addClass('editor_page').css({display: 'none'}).appendTo('body > .content')
    page.html(text).show()
    page.click 'button', (e)->
      event = $(e.target).attr('data-event')
      page.trigger(event, e.target) if event

    page.on 'wiki:close', @close_editor
    $('.content_page').hide();
    page

  close_editor: ->
    $('.content .editor_page').remove()
    $('.content .content_page').show()



class App.WikiEditorPage extends App.EditorPage

  constructor: (id)->
    @id = id

  show: (page_data)->
    page = super JST['templates/wiki/editor'](page_data)
    CKEDITOR.replace('page_editor')
    page.on 'wiki:save', => @save_page(@id)
    page.on 'wiki:preview', => @preview_page()
    page

  save_page: (id)->
    @update_ckeditor_fields()
    form = $('.editor_page form')
    $.post Routes.update_page_path({id: id}), form.serialize(), (res)=>
      @update_page_html(res.text)
      @close_editor()

  update_page_html: (text)->
    $('.content_page .page').html(text)

  preview_page: ->
    @update_ckeditor_fields()
    form = $('.editor_page form')
    $.post Routes.preview_page_path(), form.serialize(), (res)=>
      @show_preview(res.text)

  show_preview: (text)->
    $('.editor_page .page_preview').html(text).show()

  update_ckeditor_fields: ->
    for name, instance of CKEDITOR.instances
      instance.updateElement()


class App.Wiki

  show_editor: () ->
    uid = @page.uid    
    @get_source(uid).done (res)=>      
      @editor = new App.WikiEditorPage(uid)
      @editor.show(res)

  get_source: (id)->        
    url = Routes.page_path(id, {format: 'json'})    
    return $.get url, {dataType: 'json'}

window.wiki = new App.Wiki
