window.App ||= {}

class App.Announces

  constructor: (el)->
    @el = el
    @show_announces()

  show_announces: ->
    $.get Routes.api_events_path(), (res)=>
      $content = $( JST['templates/wiki/events_slider']({items: res}) )
      $images = $content.find('img')

      @load_images $images, =>
        $('.slider', @el).carouFredSel
          items: 3
          width: 840
          align: 'center'
          prev: 
            button: $('.prev', @el)
          next: 
            button: $('.next', @el)
          scroll:
            items: 1
          auto:
            play: false

      @el.html($content)

  load_images: ($images, callback) ->
    image_count = 0
    if $images.length > 0
      image_callback = ->
        if image_count < $images.length - 1
          image_count += 1
        else
          callback()

      $images.bind 'load', image_callback
      $images.bind 'error', image_callback
    else
      callback()


$ ->
  el = $('.announce .widget')  

  if el.length > 0
    announces = new App.Announces(el)
