window.App ||= {}

class App.FileUpload
  
  construct: (el)->
    $(el).html(JST['templates/upload_form']())
