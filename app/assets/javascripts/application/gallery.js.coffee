window.App ||= {}

class App.GalleryEditorPage extends App.EditorPage

  constructor: (current_page)->
    @current_page = current_page

  show: ()->
    page_id = @current_page.id
    page = super JST['templates/wiki/gallery_editor']({page_id: page_id})
    @init_images(page_id)
    $('#fileupload', page).fileupload
      dataType: 'json'
      autoUpload: true
      uploadTemplateId: false
      downloadTemplateId: false
      done: =>
        @init_images(page_id)
    
    $.get Routes.api_gallery_path({id: @current_page.id}), (res)=>      
    page 

  destroy_image: ($image) ->
    id = $image.data('id')
    $.post Routes.api_gallery_path(id), '_method': 'DELETE', (res) ->      
      $image.remove()

  init_images: (page_id)->
    self = @
    $.get Routes.api_gallery_path({id: page_id}), (res)->
      $('.editor_page .image_list').html(JST['templates/wiki/gallery_image_list']({images: res, edit_mode: true}))
      $('.editor_page .image_list .image_item .delete').click (e) ->
        e.preventDefault()
        self.destroy_image $(this).parents('.image_item:first')

class App.Gallery  
  el: null
  page: null

  constructor: (el, options)->    
    @el = el
    @options = options
    @hide_editor = options.hide_editor
    @page = options.current_page || {}    
    @show_gallery()
    @init_slider()    

  show_gallery: ->    
    @el.html JST['templates/wiki/gallery'](hide_editor: @hide_editor)
    unless @hide_editor
      @el.on 'click', '.gallery_edit_link', => @show_gallery_editor()

  show_gallery_editor: ->
    editor = new App.GalleryEditorPage(@page)
    page = editor.show()
    page

  init_slider: ->
    self = this
    
    @load_images (res) =>
      $('.slider').html(JST['templates/wiki/gallery_slider']({images:res}))
      $('.slider', @el).carouFredSel
        items: 6
        align: 'center'
        prev: 
          button: $('.main_image .prev')
        next: 
          button: $('.main_image .next')
        scroll:
          items: 1
          onAfter: (e)->
            $('.slider').trigger 'currentPosition', (pos)->
              $('.slider .image.active').removeClass('active')
              $('.slider .image#image-'+pos).addClass('active')
              $('.fp_scroll').fpScrollbar('scrollTo', pos)
              self.set_active(pos)
        auto:
          play: false

      #$('.slider').bind 'slideTo', (e, i) ->        
      #  $('.slider .image.active').removeClass('active')
      #  $('.slider .image#image-'+i).addClass('active')
      #  $('.fp_scroll').fpScrollbar('scrollTo', i)
      #  self.set_active(i)

      $('.slider', @el).on 'click', '.image', (e)->
        i = parseInt $(this).attr('id').match /[\d]+$/         
        $('.slider').trigger('slideTo', i)

      $('.fp_scroll', @el).fpScrollbar 
        max: res.length
      $('.fp_scroll', @el).on 'change', (e, value)->
        $('.slider').trigger('slideTo', Math.floor(value))

      @set_active(0)

  load_images: (callback) ->
    $.get Routes.api_gallery_path({id: @page.id}), callback

  set_active: (i)->
    $image = $('.slider #image-'+i, @el)
    $('.main_image > img', @el).attr 'src', @big_image_url($image)
    $('.main_image').removeClass('hidden')

  big_image_url: ($image) ->
    image = $image.attr('data-uid')
    Routes.api_thumb_path({file: image, size: '850x600'})

$ ->
  $('.content .page').on 'wiki:loaded', ->    
    el = $('.gallery_widget')    
    if el.length > 0
      options = {
        current_page: wiki.page
      }
      gallery = new App.Gallery(el, options)

class App.EventGallery extends App.Gallery
  load_images: (callback) ->
    callback(@options.images)

  big_image_url: ($image) ->
    console.log $image.data('normal-url')
    $image.data('normal-url')