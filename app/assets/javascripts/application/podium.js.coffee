class PodiumApp 
  
  constructor: ->
    $(document).on  'click', '.login_button', (e)=>
      @show_login()    
    $(document).on 'click', '.logout_button', (e)=>      
      @logout()
    @show_map()

  show_login: ->
    @show_modal
      title: 'Аутентификация'
      text: JST['templates/login_form']()
      buttons: [
        {class: 'submit btn-primary', text: 'Вход' }
      ]
    $('#modal_container .modal').on 'shown.bs.modal', (e)->
      modal = this
      $(this).on 'click', '.submit', ->
        promise = $.post Routes.api_login_path(), $('form', modal).serialize(), 'json'
        promise.done (res)->
          location.reload(false)
        promise.fail (xhd)->
          data = $.parseJSON(xhd.responseText)
          $('.alert_zone', modal).html(JST['templates/alert']({text: data.message})).show()

  logout: ->
    promise = $.post Routes.api_logout_path(), {}, 'json'
    promise.done (res)->
      location.reload(false)

  show_modal: (options)->
    el = $('#modal_container')
    el = $('<div>').attr('id', 'modal_container').appendTo('body') if el.length == 0
    el.html JST['templates/modal'](options)
    $('.modal', el).modal({show: true})

  show_map: ->    
    el = $('#yandex_map')
    if el.length > 0      
      $('<script>').attr('src', '//api-maps.yandex.ru/services/constructor/1.0/js/?sid=a6yAl1uxbpSnLR8PLTaI41uEpNKczXjh&id=yandex_map').appendTo($('body'))


  activate_background: (urls) ->
    current_position = 0
    f = =>
      @change_background urls[current_position]
      if current_position < urls.length - 1 then current_position += 1 else 0

    f()
    setInterval(f, 5000)

  change_background: (url) ->
    $bg = $('.page-bg')
    class_name = $bg[0].className
    $new_bg = $('<div/>').addClass(class_name).css('background-image', "url(#{url})")
    $new_bg.insertBefore $bg
    $bg.fadeOut 700, ->
      $bg.remove()

$ ->
  window.podium = new PodiumApp
