window.App ||= {}

class App.Menu
  
  el: null
  page: null  

  constructor: (el, options)->    
    @el = el
    @options = options    
    @page = options.current_page || {}
    @show_menu()

  show_menu: ->
    @el.html JST['templates/wiki/menu']

$ ->
  $('.content .page').on 'wiki:loaded', ->    
    el = $('.menu_widget')    
    if el.length > 0
      options = {
        current_page: wiki.page
      }
      menu = new App.Menu(el, options)

