(($, window) ->

  class FpScrollbar

    defaults:
      min: 0
      max: 100
      delay: 100

    constructor: (el, options) ->
      @options = $.extend({}, @defaults, options)
      @$el = $(el)
      @appendSlide()
      
    checkValue: (checkValue, minVal, maxVal)->
      res = checkValue;
      return minVal if checkValue < minVal
      return maxVal if checkValue > maxVal
      return res

    posToValue: (pos)->
      @options.min + pos * (@options.max - @options.min - 1)/@width 

    valueToPos: (value)->      
      (value - @options.min) * @width / (@options.max - @options.min - 1)

    setValue: (value)->
      @value = value
      self = this
      clearTimeout(@value_timeout) if @value_timeout
      @value_timeout = setTimeout ( ->
        self.$el.trigger('change', self.value)
      ), @options.delay

    scrollTo: (value)->      
      @slide.css('left', @valueToPos(value))

    appendSlide: -> 
      slide = $('<div>').addClass('fp_slide')
      slide.appendTo(@$el)
      slide.on('dragstart',  false)
      @slide = slide
      @width =  @$el.width() - slide.width()

      clicked = false
      pointx = 0
      dx = 0 
      self = this      
      left = @$el.offset().left      
      slide.on 'mousedown', (e)->
        pointx = e.pageX - $(this).offset().left
        clicked = true        

      $(document).on 'mouseup', slide, (e)->
        clicked = false

      $(document).on 'mousemove', slide, (e)-> 
        if clicked
          x = self.checkValue(e.pageX - left - pointx, 0, self.width)
          self.setValue(self.posToValue(x))
          slide.css('left', x)

  $.fn.extend fpScrollbar: (option, args...) ->
    @each ->
      $this = $(this)
      data = $this.data('fpScrollbar')

      if !data
        $this.data 'fpScrollbar', (data = new FpScrollbar(this, option))
      if typeof option == 'string'
        data[option].apply(data, args)

) window.jQuery, window