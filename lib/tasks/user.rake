namespace :user do

  desc "Create user with password"
  task :create => [:environment] do 
    puts "Enter login: "
    login = STDIN.gets.chomp
    puts "Enter password: "
    password = STDIN.gets.chomp
    user = User.where(login: login).first_or_create
    user.update_attribute(:password, password)
    puts "DONE\n"
  end

end
